import os
import requests
import zipfile
import psutil
import subprocess
import time

def download_file(url, file_path):
    response = requests.get(url)
    with open(file_path, 'wb') as file:
        file.write(response.content)

def extract_zip(zip_path, extract_path):
    with zipfile.ZipFile(zip_path, 'r') as zip_ref:
        zip_ref.extractall(extract_path)

def is_program_running(process_name):
    for process in psutil.process_iter(['name']):
        if process.info['name'] == process_name:
            return True
    return False

def stop_program(process_name):
    for process in psutil.process_iter(['name']):
        if process.info['name'] == process_name:
            process.kill()

def start_program(program_path):
    subprocess.Popen(program_path)

def self_update():

    print("##########################################")
    print("# Ambar - Selfupdater")
    print("##########################################")
    time.sleep(1)
    print('Iniciando a atualização do sistema')
    time.sleep(2)

    # Verificar se o programa está em execução
    if is_program_running('Ambar.exe'):
        print("O sistema ainda está em execução, finalizando processo...")
        stop_program('Ambar.exe')
        print("Processo foi finalizado, retomando a atualização")

    # URL do Ambar.zip
    update_url = "http://216.238.101.52/Ambar.zip"

    # Diretório do ERP
    diretorio = "C:\\Ambar"

    # Caminho completo para o arquivo .zip baixado
    zip_path = os.path.join(diretorio, "Ambar.zip")

    # Extrair o conteúdo para este diretório
    extract_path = diretorio

    # Baixar o novo arquivo .zip
    print('Efetuando download, aguarde um instante')
    download_file(update_url, zip_path)
    time.sleep(1)

    # Extrair o conteúdo do arquivo .zip
    print('Extraindo arquivos')
    extract_zip(zip_path, extract_path)

    # Remover o arquivo .zip baixado
    print('Limpando arquivos da instalação')
    os.remove(zip_path)

    print("Atualização foi concluída.")

    time.sleep(1)
    print("Reabrindo o sistema...")

    time.sleep(3)

    # Abrir o programa novamente
    program_path = os.path.join(diretorio, "Ambar.exe")
    start_program(program_path)

if __name__ == "__main__":
    self_update()

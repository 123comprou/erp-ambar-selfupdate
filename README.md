# ERP Ambar - Self-Updater
![SpashProject](spash.png)

Projeto desenvolvido em python para criar um selfupdater para o ERP Aton - Ambar-X.

#### Features
- [X] Verificar se o programa **Ambar.exe** está aberto
- [X] Finalizar o **Ambar.exe** se estiver aberto.
- [X] Baixar arquivo Zip com a atualização
- [X] Executar o programa ao final da execução
- [ ] Incluir mais arquivos no bundle de atualização
- [ ] Incluir o próprio self-updater nesta rotina
- [ ] Incluir outros tipos de scripts


#### Dependências
```
pip install psutil
pip install pyinstaller
```

#### Desenvolvimento
Execução do código
```
python .\src\selfupdater.py
```


#### Build
Para criar um executável do projeto
```
pyinstaller --onefile src/selfupdater.py
```

